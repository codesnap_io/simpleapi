<?php
/**
 * @package   SimpleAPI
 * @author    Uta Marian - Alexandru
 */

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'system' . DIRECTORY_SEPARATOR . 'init.php');

header('Content-Type: application/json');
echo \json_encode(\_SIMPLEAPI\_MESSAGE::_RESPONSE());
