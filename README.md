#SimpleAPI
This is a simple RESTful API build in PHP

I've build this project in order to help me create a fast mvp.


## Documentation

[Wiki documentation](https://codesnap.io/wiki/simple-api/)

## Support

For answers you might not find in the Wiki, feel free to send me an email at *hello@codesnap.io*.