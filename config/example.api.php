<?php
/**
 * @package   SimpleAPI
 * @author    Uta Marian - Alexandru
 */

/**
 * Api configuration
 * Restrictions
 * Allowed tests
 * Enabled modules
 */
return [
    // Api Version
    'version' => '1.0',
    // Secure Token
    '_SECURE' => '',
    // Set server timezone
    '_TIMEZONE' => ''
];