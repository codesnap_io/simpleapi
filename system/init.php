<?php
/**
 * @package   SimpleAPI
 * @author    Uta Marian - Alexandru
 */

/**
 * Load the local configuration file
 */
$__DIRECTORIES = [
    '_ROOT' => dirname(__FILE__) . DIRECTORY_SEPARATOR,
    '_CONFIG' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR,
    '_MODULE' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'module' . DIRECTORY_SEPARATOR,
    '_CLASSES' => dirname(__FILE__) . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR,
    '_LOGS' => dirname(__FILE__) . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR,
    '_CRONS' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../_crons' . DIRECTORY_SEPARATOR,
    //:: modules
    '_MODULE_CONFIG' => DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . '_config.php',
    '_MODULE_DATABASE' => DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . '_database' . DIRECTORY_SEPARATOR . '*',
    '_MODULE_CLASS_FILE' => DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Module.php',
    '_MODULE_ROUTE' => DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . '_routing.php',
];

/**
 * System initialization
 */
require_once $__DIRECTORIES['_ROOT'] . '_simpleapi.class.php';
foreach (glob($__DIRECTORIES['_ROOT'] . '_classes' . DIRECTORY_SEPARATOR . "*.system.php") as $filename):
    require_once $filename;
endforeach;


/**
 * Configs load
 */
//:: API
$_API_CONFIG = \_SIMPLEAPI\_CONFIG::_LOAD(['_FILE_LOCATION' => $__DIRECTORIES['_CONFIG'] . 'api.php']);

/**
 * Set the timezone
 */
if(!empty($_API_CONFIG['_TIMEZONE']))
    date_default_timezone_set($_API_CONFIG['_TIMEZONE']);


//:: ERROR CODES
$_ERROR_CODE = \_SIMPLEAPI\_CONFIG::_LOAD(['_FILE_LOCATION' => $__DIRECTORIES['_CONFIG'] . 'error.code.php']);
//:: DEFAULT DB
$_DATABASE = \_SIMPLEAPI\_CONFIG::_LOAD(['_FILE_LOCATION' => $__DIRECTORIES['_CONFIG'] . 'database.php']);

/**
 * Load helpers
 */
foreach (glob($__DIRECTORIES['_ROOT'] . '_classes' . DIRECTORY_SEPARATOR . '_helpers' . DIRECTORY_SEPARATOR . "*.helper.php") as $filename):
    require_once $filename;
endforeach;


foreach ($_DATABASE as $key => $_DB):
    $_CONN[$key] = new DB([
        "_DB_HOSTNAME" => $_DB['hostname'],
        "_DB_USERNAME" => $_DB['username'],
        "_DB_PASSWORD" => $_DB['password'],
        "_DB_DATABASE" => $_DB['database'],
        "_DB_CHARSET" => $_DB['charset']
    ]);
endforeach;

/**
 * Load available modules
 */
$_ROUTING = [];
foreach (glob($__DIRECTORIES['_MODULE'] . "*") as $module):
    $_ROUTE = [];
    $_CONFIG = [];

    /**
     * Load custom configuration, like database connection
     */
    if (file_exists($module . $__DIRECTORIES['_MODULE_CONFIG']))
        $_CONFIG = \_SIMPLEAPI\_CONFIG::_LOAD(['_FILE_LOCATION' => $module . $__DIRECTORIES['_MODULE_CONFIG']]);

    /**
     * Load table name and set the primary key
     */
    $_CONNECTION = isset($_CONFIG['database']['default']) ? $_CONN[$_CONFIG['database']['default']] : $_CONN['default'];

    foreach (glob($module . $__DIRECTORIES['_MODULE_DATABASE']) as $module_db):
        require_once $module_db;
    endforeach;

    /**
     * Main module class
     */
    if (file_exists($module . $__DIRECTORIES['_MODULE_CLASS_FILE']))
        require_once $module . $__DIRECTORIES['_MODULE_CLASS_FILE'];

    /**
     * Module routing with response codes
     */
    if (file_exists($module . $__DIRECTORIES['_MODULE_ROUTE']))
        $_ROUTE = \_SIMPLEAPI\_CONFIG::_LOAD(['_FILE_LOCATION' => $module . $__DIRECTORIES['_MODULE_ROUTE']]);
    $_ROUTING = array_merge($_ROUTING, $_ROUTE);
endforeach;