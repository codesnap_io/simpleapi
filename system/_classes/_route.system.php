<?php
/**
 * @package   SimpleAPI
 * @author    Uta Marian - Alexandru
 */

namespace _SIMPLEAPI;
class _ROUTE extends \_SIMPLEAPI\_INIT
{
    static public function _LIST()
    {
        global $_ROUTING;
        return $_ROUTING;
    }
}