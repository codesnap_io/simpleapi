<?php
/**
 * @package   SimpleAPI
 * @author    Uta Marian - Alexandru
 */

namespace _SIMPLEAPI;
class _REQUEST extends \_SIMPLEAPI\_INIT
{
    /**
     * Get Method
     */
    static function _METHOD()
    {
        return strtolower(\_SIMPLEAPI\_SANITIZE::input($_SERVER['REQUEST_METHOD']));
    }

    /**
     * Return URI
     */
    static function _URI()
    {
        $_REQUEST_URI = \_SIMPLEAPI\_SANITIZE::input($_SERVER['REQUEST_URI']);
        return [
            "_REQUEST" => $_REQUEST_URI,
            "_ROUTE" => explode("?", ltrim($_REQUEST_URI, '/')),
            "_EXTEND" => array_filter(explode("/", explode("?", ltrim($_REQUEST_URI, '/'))['0']))
        ];
    }

    /**
     * Return POST data
     */
    static function _POST()
    {
        return \_SIMPLEAPI\_SANITIZE::input($_POST);
    }

    /**
     * Route details
     *
     * Return current route details based on request
     */
    static function _ROUTE()
    {
        return \_SIMPLEAPI\_ROUTE::_LIST()[\_SIMPLEAPI\_REQUEST::_URI()['_ROUTE']['0']];
    }

}


