<?php
/**
 * @package   SimpleAPI
 * @author    Uta Marian - Alexandru
 */

namespace _SIMPLEAPI;
class _TEMPLATE extends \_SIMPLEAPI\_INIT
{
    public $vars = array();

    /**
     * Get the variable
     * @param $_VAR
     * @return mixed
     */
    public function __get($_VAR)
    {
        return $this->vars[$_VAR];
    }

    /**
     * Set the variable
     * @param $_VAR
     * @param $_VALUE
     * @throws Exception
     */
    public function __set($_VAR, $_VALUE)
    {
        if ($_VAR == 'view_template_file') {
            throw new Exception("Cannot bind variable named 'view_template_file'");
        }
        $this->vars[$_VAR] = $_VALUE;
    }

    /**
     * Render the template
     * @param $_TEMPLATE_FILE
     * @return string
     * @throws Exception
     */
    public function render($_TEMPLATE_FILE)
    {
        if (array_key_exists('view_template_file', $this->vars)) {
            throw new Exception("Cannot bind variable called 'view_template_file'");
        }
        extract($this->vars);
        ob_start();
        include_once($_TEMPLATE_FILE);
        $_LOAD = ob_get_contents();
        //$_LOAD = preg_replace('~\{\$(\w+?)\}~sUe', '${"$1"}', $_LOAD);//JUST IF YOU WANT TO ENABLE SHORT TAGS
        ob_get_clean();
        return $_LOAD;
    }
}