<?php
/**
 * @package   SimpleAPI
 * @author    Uta Marian - Alexandru
 */

namespace _SIMPLEAPI;
class _STRING extends \_SIMPLEAPI\_INIT
{

    /**
     * string
     * limit
     */
    static public function _substr($obj)
    {
        $obj['string'] = \_SIMPLEAPI\_SANITIZE::input($obj['string']);
        $end = (strlen($obj['string']) > $obj['limit']) ? '...' : '';
        return substr($obj['string'],0,$obj['limit']) . $end;

    }


}