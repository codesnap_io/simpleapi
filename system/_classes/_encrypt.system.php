<?php
/**
 * @package   SimpleAPI
 * @author    Uta Marian - Alexandru
 */

namespace _SIMPLEAPI;
class _ENCRYPT extends \_SIMPLEAPI\_INIT
{
    const
        _SECRET_IV = 'KJASLK@J#KA#15ASJ',
        _ENCRYPT_METHOD = "AES-256-CBC";

    public static function _ENCRYPT($_DATA)
    {
        //:: hash
        $_KEY = hash('sha256', $_DATA['_SECRET']);

        //:: iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $_IV = substr(hash('sha256', self::_SECRET_IV), 0, 16);
        return base64_encode(openssl_encrypt($_DATA['_STRING'], self::_ENCRYPT_METHOD, $_KEY, 0, $_IV));
    }

    public static function _DECRYPT($_DATA = [])
    {
        //:: hash
        $_KEY = hash('sha256', $_DATA['_SECRET']);
        //:: iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $_IV = substr(hash('sha256', self::_SECRET_IV), 0, 16);

        return openssl_decrypt(base64_decode($_DATA['_STRING']), self::_ENCRYPT_METHOD, $_KEY, 0, $_IV);
    }


    /**
     * Generate unique hash code
     */
    public static function _HASH_CODE($_DATA = [])
    {
        $_DATA['_TYPE'] = isset($_DATA['_TYPE']) ? $_DATA['_TYPE'] : 'ALL';
        $_DATA['_LENGTH'] = isset($_DATA['_LENGTH']) ? $_DATA['_LENGTH'] : '12';
        /*
         *
         * $_DATA['_LENGTH']
         */
        switch ($_DATA['_TYPE']) {
            case 'TOKEN':
                $_STRING = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'TOKEN_SECRET':
                $_STRING = '/abcdefghijklm.nopqrstuvwxyz/ABCDEFGHIJKL.MNOPQRSTUVWXYZ/01234.56789';
                $_STRING = self::_HASH_CODE(['_LENGTH' => '50', '_TYPE' => 'ALPHA']);
                $_STRING .= self::_HASH_CODE(['_LENGTH' => '50', '_TYPE' => 'HEXDEC']);
                $_STRING .= self::_HASH_CODE(['_LENGTH' => '50', '_TYPE' => 'COMBINED']);
                $_STRING .= self::_HASH_CODE(['_LENGTH' => '50', '_TYPE' => 'DISTINCT']);
                $_STRING .= self::_HASH_CODE(['_LENGTH' => '50', '_TYPE' => 'NUMERIC']);
                $_STRING .= self::_HASH_CODE(['_LENGTH' => '50', '_TYPE' => 'NOZERO']);
                break;
            case 'COMBINED':
                $_STRING = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'ALPHA':
                $_STRING = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'HEXDEC':
                $_STRING = '0123456789abcdef';
                break;
            case 'NUMERIC':
                $_STRING = '0123456789';
                break;
            case 'NOZERO':
                $_STRING = '123456789';
                break;
            case 'DISTINCT':
                $_STRING = '2345679ACDEFHJKLMNPRSTUVWXYZ';
                break;
            default:
                $_STRING = (string)$_DATA['_TYPE'];
                break;
        }

        $_STRING .= session_id()
            . md5(rand(0, 9)
                . strtotime("now"))
            . rand(10, 20)
            . (rand(21, 30) * rand() * md5(strtotime("now") * rand(31, 40)));
        $crypto_rand_secure = function ($min, $max) {
            $range = $max - $min;
            if ($range < 0)
                return $min; // not so random...
            $log = log($range, 2);
            $bytes = (int)($log / 8) + 1; // length in bytes
            $bits = (int)$log + 1; // length in bits
            $filter = (int)(1 << $bits) - 1; // set all lower bits to 1
            do {
                $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
                $rnd = $rnd & $filter; // discard irrelevant bits
            } while ($rnd >= $range);
            return $min + $rnd;
        };

        $_TOKEN = "";
        $max = strlen($_STRING);
        for ($i = 0; $i < $_DATA['_LENGTH']; $i++) {
            $_TOKEN .= $_STRING[$crypto_rand_secure(0, $max)];
        }
        return $_TOKEN;
    }


}