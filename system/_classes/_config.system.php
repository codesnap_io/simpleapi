<?php
/**
 * @package   SimpleAPI
 * @author    Uta Marian - Alexandru
 */

namespace _SIMPLEAPI;
class _CONFIG extends \_SIMPLEAPI\_INIT
{
    /**
     * Load configuration file
     */
    static public function _LOAD($_DATA = [])
    {
        if (file_exists($_DATA['_FILE_LOCATION']))
            return require $_DATA['_FILE_LOCATION'];
        else
            return [];
    }
}