<?php
/**
 * @package   SimpleAPI
 * @author    Uta Marian - Alexandru
 */

namespace _SIMPLEAPI;
class _ERROR extends \_SIMPLEAPI\_INIT
{
    static public function _LIST()
    {
        global $_ERROR_CODE;
        return $_ERROR_CODE;
    }
}