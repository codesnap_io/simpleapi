<?php
/**
 * @package   SimpleAPI
 * @author    Uta Marian - Alexandru
 */

namespace _SIMPLEAPI;
class _MESSAGE extends \_SIMPLEAPI\_INIT
{
    /**
     * Get the response
     */
    static public function _RESPONSE()
    {
        return self::_VALIDATE_URI();
    }

    /**
     * Validate URI
     * If empty or invalid return error
     * ELSE, return the message from the module
     */
    static public function _VALIDATE_URI()
    {
        $_URI = \_SIMPLEAPI\_REQUEST::_URI();
        if (empty($_URI['_REQUEST']) or $_URI['_REQUEST'] == '/')
            return self::_URI_NOT_FOUNT();
        else
            return self::_URI_MODULE_VALIDATE();
    }

    /**
     * Default message if route not found
     */
    static private function _URI_NOT_FOUNT()
    {
        return [
            'code' => \_SIMPLEAPI\_ERROR::_LIST()['routing']['invalid']['code'],
            'message' => \_SIMPLEAPI\_ERROR::_LIST()['routing']['invalid']['message']
        ];
    }

    /**
     * Validate custom module
     */
    static private function _URI_MODULE_VALIDATE()
    {
        /**
         * If static route exist
         */
        if (isset(\_SIMPLEAPI\_ROUTE::_LIST()[\_SIMPLEAPI\_REQUEST::_URI()['_ROUTE']['0']]))
            return self::_URI_MODULE_STATIC();
        else
            /**
             * Search for a dynamic route /route/{id}
             */
            return [
                'code' => \_SIMPLEAPI\_ERROR::_LIST()['routing']['nonexistent']['code'],
                'message' => \_SIMPLEAPI\_ERROR::_LIST()['routing']['nonexistent']['message']
            ];
    }

    /**
     * Get the response from the module
     */
    static private function _URI_MODULE_STATIC()
    {
        /**
         * Get route details
         */
        $_ROUTE_MODE = \_SIMPLEAPI\_ROUTE::_LIST()[\_SIMPLEAPI\_REQUEST::_URI()['_ROUTE']['0']];
        /**
         * Validate the request mode
         */
        if (in_array(\_SIMPLEAPI\_REQUEST::_METHOD(), $_ROUTE_MODE['methods'])):
            $_METHOD = \_SIMPLEAPI\_REQUEST::_METHOD() . $_ROUTE_MODE['action'] . \_SIMPLEAPI\_INIT::_ACTION;
            $_MODULE_NAME = '\_MODULE\\' . $_ROUTE_MODE['module'];
            //:: module init
            $_MODULE = new $_MODULE_NAME();

            /**
             * Validate request
             * method + action
             */
            if (method_exists($_MODULE, $_METHOD))
                return $_MODULE->$_METHOD();
            else
                return [
                    'code' => \_SIMPLEAPI\_ERROR::_LIST()['routing']['forbidden']['not_existed']['code'],
                    'message' => \_SIMPLEAPI\_ERROR::_LIST()['routing']['forbidden']['not_existed']['message']
                ];
        else:
            /**
             * to-do
             * check if a dynamic route exist
             */
            return [
                'code' => \_SIMPLEAPI\_ERROR::_LIST()['routing']['forbidden']['not_allowed']['code'],
                'message' => \_SIMPLEAPI\_ERROR::_LIST()['routing']['forbidden']['not_allowed']['message']
            ];
        endif;
    }
}