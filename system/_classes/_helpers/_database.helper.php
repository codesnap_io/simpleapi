<?php
/**
 * @package   SimpleAPI
 * @author    Uta Marian - Alexandru
 */

/**
 * Database helper
 *
 * e.g.
 *
 * $module = new \_MODULE\_DB\Users();
 *
 * //:: add new data
 * $module->name = 'Name';
 * $module->status = 1;
 * $module_save = $module->create();
 *
 * //:: delete
 * $module->id = '1';
 * $module_delete = $module->delete();
 *
 * //:: save
 * $module->name = 'New name';
 * $module->status = 2;
 * $module_save = $module->save();
 *
 * //:: find
 *
 * $module->id = 1;//or, $module->name = 'Name';
 * $module->find();
 *
 *
 */
namespace _SIMPLEAPI;
class _CRUD extends \_SIMPLEAPI\_INIT
{
    private $db;
    public $variables;
    public $connSettings;

    public function __construct($data = array())
    {
        global $_CONNECTION;
        $this->db = $_CONNECTION;
        $this->variables = $data;
    }

    public function __set($name, $value)
    {
        if (strtolower($name) === $this->key) {
            $this->variables[$this->key] = $value;
        } else {
            $this->variables[$name] = $value;
        }
    }

    public function __get($name)
    {
        if (is_array($this->variables)) {
            if (array_key_exists($name, $this->variables)) {
                return $this->variables[$name];
            }
        }
        return null;
    }

    /**
     * Data save
     */
    public function save($id = "0")
    {
        $this->variables[$this->key] = (empty($this->variables[$this->key])) ? $id : $this->variables[$this->key];
        $fieldsvals = '';
        $columns = array_keys($this->variables);
        foreach ($columns as $column):
            if ($column !== $this->key)
                $fieldsvals .= $column . " = :" . $column . ",";
        endforeach;

        $fieldsvals = substr_replace($fieldsvals, '', -1);
        if (count($columns) > 1):
            $sql = "UPDATE " . $this->table . " SET " . $fieldsvals . " WHERE " . $this->key . "= :" . $this->key;
            if ($id === "0" && $this->variables[$this->key] === "0"):
                unset($this->variables[$this->key]);
                $sql = "UPDATE " . $this->table . " SET " . $fieldsvals;
            endif;
            return $this->exec($sql);
        endif;
        return null;
    }


    public function create()
    {
        $bindings = $this->variables;
        if (!empty($bindings)) {
            $fields = array_keys($bindings);
            $fieldsvals = array(implode(",", $fields), ":" . implode(",:", $fields));
            $sql = "INSERT INTO " . $this->table . " (" . $fieldsvals[0] . ") VALUES (" . $fieldsvals[1] . ")";
        } else {
            $sql = "INSERT INTO " . $this->table . " () VALUES ()";
        }
        return ['exec' => $this->exec($sql), 'id' => $this->db->lastInsertId()];
    }

    public function delete($id = "")
    {
        $id = (empty($this->variables[$this->key])) ? $id : $this->variables[$this->key];

        $delete_key = self::_options($this->connSettings)['delete_key'];
        $delete_value = self::_options($this->connSettings)['delete_value'];

        $this->key = $delete_key ? $delete_key : $this->key;
        $id = $delete_value ? $delete_value : $id;

        if (!empty($id)):
            $sql = "DELETE FROM " . $this->table . " WHERE " . $this->key . "= :" . $this->key;// . " LIMIT 1"
        endif;
        return $this->exec($sql, array($this->key => $id));
    }

    public function find($id = "")
    {
        $id = (empty($this->variables[$this->key])) ? $id : $this->variables[$this->key];

        if (!empty($id)):
            $sql = "SELECT * FROM " . $this->table . " WHERE " . $this->key . "= :" . $this->key . " LIMIT 1";
            $result = $this->db->row($sql, array($this->key => $id));
            $this->variables = ($result != false) ? $result : null;
        endif;
    }

    /**
     * @param array $fields .
     * @param array $sort .
     * @return array of Collection.
     * Example: $user = new User;
     * $found_user_array = $user->search(array('sex' => 'Male', 'age' => '18'), array('dob' => 'DESC'));
     * // Will produce: SELECT * FROM {$this->table_name} WHERE sex = :sex AND age = :age ORDER BY dob DESC;
     * // And rest is binding those params with the Query. Which will return an array.
     * // Now we can use for each on $found_user_array.
     * Other functionalities ex: Support for LIKE, >, <, >=, <= ... Are not yet supported.
     */
    public function search($fields = array(), $sort = array(), $limit = '')
    {
        $bindings = empty($fields) ? $this->variables : $fields;
        $sql = "SELECT " . self::_options($this->connSettings)['select'] . " FROM " . $this->table;

        if (!empty($bindings)) {
            $this->variables = $bindings;
            $fieldsvals = array();
            $columns = array_keys($bindings);
            foreach ($columns as $column) {


                if(isset($bindings[$column])):
                    $fieldsvals [] = $column . " " . $bindings[$column]['operator']  ." :" . $column;
                else:
                    $fieldsvals [] = $column . " = :" . $column;
                endif;
            }
            $sql .= " WHERE " . implode(" AND ", $fieldsvals);
        }

        if (!empty($sort)) {
            $sortvals = array();
            foreach ($sort as $key => $value) {
                $sortvals[] = $key . " " . $value;
            }
            $sql .= " ORDER BY " . implode(", ", $sortvals);
        }
        $sql .= empty($limit) ? '' : ' limit ' . $limit;
        return $this->exec($sql);
    }

    public function all()
    {
        return $this->db->query("SELECT " . self::_options($this->connSettings)['select'] . " FROM " . $this->table);
    }


    static private function _options($obj)
    {
        return [
            'select' => isset($obj['select']) ? $obj['select'] : '*',
            'delete_key' => isset($obj['delete_key']) ? $obj['delete_key'] : false,
            'delete_value' => isset($obj['delete_value']) ? $obj['delete_value'] : false,
        ];
    }


    public function min($field)
    {
        if ($field)
            return $this->db->single("SELECT min(" . $field . ")" . " FROM " . $this->table);
    }

    public function max($field)
    {
        if ($field)
            return $this->db->single("SELECT max(" . $field . ")" . " FROM " . $this->table);
    }

    public function avg($field)
    {
        if ($field)
            return $this->db->single("SELECT avg(" . $field . ")" . " FROM " . $this->table);
    }

    public function sum($field)
    {
        if ($field)
            return $this->db->single("SELECT sum(" . $field . ")" . " FROM " . $this->table);
    }

    public function count($field)
    {
        if ($field)
            return $this->db->single("SELECT count(" . $field . ")" . " FROM " . $this->table);
    }

    private function exec($sql, $array = null)
    {
        if ($array !== null) {
            // Get result with the DB object
            $result = $this->db->query($sql, $array);
        } else {
            // Get result with the DB object
            $result = $this->db->query($sql, $this->variables);
        }

        // Empty bindings
        $this->variables = array();
        return $result;
    }
}